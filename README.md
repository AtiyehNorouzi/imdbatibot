Imdb Telegram Bot  (@imdb_atibot)
 
 What this bot do?
 
 This bot retrieves movie datas from parsing html pages of IMDb (www.imdb.com) site. Users can search movies and see box-office chart using this bot.
 
 Search Movies :
 there are two buttons (next movie, IMDB Link) for each movie. if the recieved movie wasn't the same movie you were looking for, press Next Movie Button.
 following that the next related movie to your search will be showed.
 
![bot](/uploads/629d6c3f7594e961f596a614a09c5313/bot.PNG)

![ds](/uploads/046a3b73a2f547b8cf0106f2208ee2e4/ds.jpg)
![ds2](/uploads/fb73e7e1b05c54255b9f216c8f37657b/ds2.jpg)


See Box-Office chart

![box](/uploads/e4196f4fc421121a88ec2c498185ff6f/box.jpg)

